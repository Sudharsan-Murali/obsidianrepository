==2023-02-06 12:31:PM==

---

# Power Automate

[Testing], [Learning], [Automation], [Flow], [Power Automate]

CERTIFICATION: [Exam PL-500: Microsoft Power Automate RPA Developer - Certifications | Microsoft Learn](https://learn.microsoft.com/en-us/certifications/exams/pl-500)

[[Automate processes with Robotic Process Automation and Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/work-automation-flow/)
[[Get started with Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/pad-get-started/)
[[Work with Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/pad-work/)
[[Work with different technologies in Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/pad-work-different-technologies/)
[[Implement advanced logic in Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/pad-implement-advanced-logic/)
[[Build expertise with Power Automate for desktop]] (https://learn.microsoft.com/en-us/training/paths/pad-build-expertise/)
[[Get started with custom connectors in Power Automate]] (https://learn.microsoft.com/en-us/training/modules/get-started-custom-connector/)
[[Configure custom connectors with authenticated APIs in Power Automate]] (https://learn.microsoft.com/en-us/training/modules/configure-custom-connectors-api/)
[[Introduction to Microsoft Power Platform security and governance]] (https://learn.microsoft.com/en-us/training/modules/security-governance-intro/)