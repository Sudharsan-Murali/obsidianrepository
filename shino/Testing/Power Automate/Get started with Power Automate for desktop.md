==2023-02-06 12:58:PM==

# Get Started With Power Automate for Desktop
---

## Take Your First Steps with Power Automate for Desktop

### Introduction:
The Microsoft Power Automate for desktop platform consists of two main components that enable you to create and manage flows: the console and the flow designer.

The console is the main control panel of the platform from which you can launch the flow designer to create or edit flows. Through the console's options, you can also delete, rename, and run existing flows.

![Console Page | 800](https://learn.microsoft.com/en-us/training/modules/pad-first-steps/media/console.png#lightbox)

The flow designer is the development environment of Power Automate for desktop, where you can build flows and perform debugging. To develop a flow, you can drag and drop in the main workspace any of the available actions.

![Flow Page | 800](https://learn.microsoft.com/en-us/training/modules/pad-first-steps/media/flow-designer.png#lightbox)

### Console Overview:
When you select **My flows**, you see the console. The console is the main component of the platform from which you can manage your existing flows and launch the flow designer to edit them or create new ones. When you select an existing flow, you get a new command bar with bulk actions you can perform.

To handle an existing flow, right-click on it and choose between starting, editing, renaming, and deleting it. If the flow is currently running, one more option to stop it is available.

### Flow Designer Overview:
When you choose to create or edit a flow, Power Automate for desktop launches the flow designer. The flow designer is the platform's primary development tool with which you develop flows that deploy actions. The **flow designer consists of seven components** that enable you to manage every aspect of the development process.

The actual development happens in **the workspace**, which is the main component of the window. By using the workspace, you can develop flows that run sequentially or alter the run path by using loops, conditionals, and flow control actions.

To deploy or search for an action, you can use **the Actions pane** located on the left side of the designer. All actions are categorized into groups based on their functionality.

While automating business procedures, you might create complex flows that contain numerous actions. Power Automate for desktop allows you to simplify these flows and makes testing easier through subflows. **Subflows** are groups of actions that you can reference as a group within other subflows. Every flow contains the main subflow that runs automatically when the flow starts.

> [!HINT]
> To create a new subflow, select the plus (**+**) icon on the **Subflows** tab above the workspace and then enter the desired name.

Every action that is used in a flow accepts some input parameters and stores its results in new variables. The platform displays all produced variables in **the Variables pane**, where you can search for variables, rename them, find their usages, and filter them by type. Except for local variables that are used only in the current flow, you can create input/output variables to pass data to and from Power Automate for desktop. Input and output variables empower you to combine multiple Power Automate products and create a powerful and robust automation solution.

In scenarios where you use the UI and web automation capabilities of Power Automate for desktop, you can use **the UI elements tab** to manage the existing UI elements or create new ones. All UI elements that your flow accesses, such as buttons and checkboxes, are stored on this tab.

The last main component of the flow designer is **the Images tab**. This tab stores all images that are used in the deployed actions, allowing you to access and manage them effortlessly.

During the development or implementation of the flow, you might encounter different types of errors. When an error happens, the platform displays **the Errors and Warnings pane**, and provides you with all available information to resolve it. You can use this feature to debug your flows and identify existing inconsistencies.

## Power Automate for Desktop - Development Essentials