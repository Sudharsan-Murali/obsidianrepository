==2023-02-02 23:34:PM==
# Exploratory Data Analysis
---

The initial data analysis was made to check the ratio of existence of heart disease among the selected individuals of the datasset. 
By plotting the data in a bar plot, it is deducible that the severity of the disease is inversely proportional to the rate of occurence of the disease. Thus, patients with no heart disease are in the majority, while patients with severe form of the heart disease are few in number.

![[Pasted image 20230202233423.png]]