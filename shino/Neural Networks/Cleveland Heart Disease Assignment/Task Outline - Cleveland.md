==2023-02-01 23:42:PM==
# Task Outline
## Applicable Atrificial Intelligence - Data Classification using Neural Networks
---
## Task Outline:
The objective of the coursework is to design, implement and evaluate a Neural Network for data classification and write a report on it. The neural network is to be developed using Matlab.

---

## The Data:
A subset of the Heart Disease (Cleveland) data set is provided via Blackboard (file named cleveland_heart_disease_dataset_labelled.mat). The data has the following properties:

-   This is a cleaned up subset of 14 features from a full set of 75,
-   It contains multiple classes (0: no heart disease, 1: mild heart disease, 2: severe heart disease),
-   The majority of experiments in the literature focus on detecting presence (1 or 2) from absence (0),
-   Current state of the art is around 90% accuracy.

The task is to design a neural network to achieve a cross validated classification rate as close as possible to current state of the art.

---

## Deliverables:
A report is required providing a full description and evaluation of the work done. The report should be structured as follows:

-   Introduction: description of the task and preliminary data analysis    
-   Requirements analysis: WHAT the system is required to do, functional and non-functional requirements, expected performance, etc.
-   Design considerations: HOW the requirements will be achieved, method, data structure, exploratory data analysis, code modularity, usability, etc.
-   Implementation and testing: coding decisions, test cases, test results, performance
-   Evaluation/conclusion: with critical appraisal of method and results
-   References: use Harvard notation
-   Appendix: all Matlab code go into the appendix

**Guidance:** it is suggested around 2,000+ words covering all items as indicated above (figures, diagrams, and appendix do not count). You can use code excerpts to explain your design and implementation decisions in the main body of the report, but the full Matlab code must be included in the appendix.

---

## Submission:
This is an individual project.
**Report hand-in date: 3PM on Thursday 20 April 2022 via Blackboard.**

---
[[Neural Networks]], [[Dataset Analysis - Cleveland]], [[Exploratory Data Analysis - Cleveland]], [[Citations - Cleveland]]
