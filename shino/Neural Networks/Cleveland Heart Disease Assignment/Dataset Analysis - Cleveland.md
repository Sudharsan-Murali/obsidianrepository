==2023-02-02 02:03:AM==
# Dataset Analysis
---

The given dataset file **cleveland_heart_disease_dataset_labelled.mat** consists of three different spreadsheets / tables within it named *'labels'*, *'t'*, and *'x'*.

### Labels:

The *labels* table consists of the labels of the severity of the heart disease. The levels of severity is stated as:
	- **0** : *'normal'*
	- **1** : *'mild heart disease'*
	- **2** : *'severe heart disease'*

---

### t:

The *'t'* table is the numerical counterpart of the *'labels'* table. While the *'labels'* table contains the data in the form of *Strings*, the *'t'* table contains the same data in the form of *integers*.

---

### x:

The *'x'* table contains 13 parameters namely:
- *age* : age of the patient in years
- *sex* : 1 - Male, 0 - Female
- *cp* : Chest pain type
	-  Value 1: Typical Angina
	-  Value 2: Atypical Angina
	-  Value 3: non-anginal pain
	-  Value 4: Asymptomatic
- *trestbps* : resting blood pressure (in mm Hg on admission to the hospital) 
- *chol* : serum cholesterols in mg/dl
- *fbs* : fasting blood sugar > 120 mg/dl (1 = true; 0 = false)
- *restecg* : resting electrocardiographic results
	- Value 0: normal
	- Value 1: having ST-T wave abnormality (T wave inversions and/or ST elevation or depression of > 0.05 mV)
	- Value 2: showing probable or definite left ventricular hypertrophy by Estes’ criteria
- *thalach* : maximum heart rate achieved
- *exang* : exercise induced angina (1 = yes; 0 = no)
- *oldpeak* : ST depression induced by exercise relative to rest
- *slope* : the slope of the peak exercise ST segment
	- Value 1: upsloping
	- Value 2: flat
	- Value 3: Downsloping
- *ca* : number of major vessels (0–3) colored by fluoroscopy
- *thal* : 3 = normal; 6 = fixed defect; 7 = reversible defect