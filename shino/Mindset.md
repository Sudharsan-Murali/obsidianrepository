# Mindset:
#### Way of Thinking:
The gist is that a person cannotlearn each and every single thing in any field. As such, the fcus should be on improving one self rating than striving for perfection.

Understanding one programing language in depth will allow us to learn other languages faster. We do not need to learn every programming language to understand how to read their code. All of them follow the same principles that **R.D Tennet** initially defined:

1. The Principle of Abstraction
2. The Principle of Correspondence
3. The Principle of Data Type Completeness

In Information Technology, we need to:
- **Learn and Understand** these principles, structures, and processes quickly
- **Adapt our knowledge** to the various environments we enounter
- **When we don't understand** how somehing works, we have to **find out what we dont know**

Within HackTheBox, we can use the communities:
Forum - https://forum.hackthebox.eu
Discord - https://discord.gg/hackthebox

**Learn to find, choose, and adapt the info we need**

#### Think Outside The Box:
**Always think 'Outside the Box'**

#### Occam's Razor:
Think outside the box helps us recognize options and possibilities that weren't recognizable at first.Thereby, we will encounter many options, and the whole way to the solution of a problem can become very complicated very fast. The Occam's razor principle is useful in counteracting this situation:

>`The most straightforward theory is preferable to all others of several sufficient possible explanations for the same state of facts. In other words: The simplest explanation is always the most probable.`

**The simplest explanation for the best approach to penetration testing is that we work with the information we can get.**

The art, after all, is not to get some flag but to find the way to it.
**`The Destination is not as important as the Journey`**

#### Talent:
Though a lot can be spoken on this topic, my advise to myself is:
`Disregard talent. Consider yourself as Untalented. Strive to achieve through Hardwork.`

---
