---
Cssclass: cards
---
# To Read:

```dataview
TABLE Author, ("![|100](" + Cover + ")") as Cover, Category as Genre
From "Books/Book Shelf"
where contains(Status, "WishList")
```

# Completed:

```dataview
TABLE Author, ("![|100](" + Cover + ")") as Cover, "Total Pages: "+Pages, Category as Genre
From "Books/Book Shelf"
where contains(Status, "Completed")
```
# In Progress:

```dataview
TABLE Author, ("![|100](" + Cover + ")") as Cover, Category as Genre
From "Books/Book Shelf"
where contains(Status, "In Progress")
```