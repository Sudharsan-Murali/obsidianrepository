# Keyboard
**Creation Date:** 2022-12-04    **Creation Time:** 02:18:AM

-------------------
Tags: [[Arduino]], [[KMS]], [[HID Devices]]
References: [Arduino Keyboard Reference - Keyboard Modifiers](https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/)

### MOD Keys and Special Keys:
1. Function Keys --> KEY F(functionNumber) - without the rounded brackets 
2. Left Ctrl Key --> KEY_LEFT_CTRL
3. Right Ctrl Key --> KEY_RIGHT_CTRL
4. Left Alt Key --> KEY_LEFT_ALT
5. Right Alt Key --> KEY_RIGHT_ALT
6. Left Windows Key --> KEY_LEFT_GUI
7. Right Windows Key --> KEY_RIGHT_GUI
8. Left Shift Key --> KEY_LEFT_SHIFT
9. Right Shift Key --> KEY_RIGHT_SHIFT
