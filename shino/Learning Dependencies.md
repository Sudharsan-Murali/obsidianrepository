# Learning Dependencies:
#### Way of Learning:
> `A problem is an emotional state. Without emotions, it is just a situation.`

#### Learning Efficiency:
We have to understand how things work, how they are structured, and how to use them.
The primary and most difficult objective we must overcome is the `combination` of our **knowledge, adaptation, and new information**.

It often is not easy to find the information we need. First, we have to find out what kind of information we need.

-   `What do we already know?`
-   `What do we not know yet?`

First of all, we have to fail. It is an `unavoidable` and `essential` part of `learning`.

`Experience is built on failures.`

In Academy, we will learn not only the basics of penetration testing but also how to:
-   Learn faster
-   Structure our knowledge
-   Find the information we need
-   Get the overview

>`To be good at something means we know` **`what we are doing`**``. If we know what we are doing, that means that we are` **`experienced`** `with this topic. Experience means we have a vast`` **`repertoire`** `in this field. Repertoire comes from` **`associations`** `and` **`practical experience`**`. When we say practical experience, we want to know how much we have to practice to become competent at a specific task.`

#### Learning Types:
Types:
- Passive Learning
	- Learning by reading information and watching others pratise the subject.
- Active Learning
	- Practising on our own and comparing results with peers

Efficiency depends not only on the quality of information we find but on the usage of that information. Moreover, it depends on our `motivation`, `focus`, and `our goal`.

`Progress is noticeable when the question that tortured us has lost its meaning.`

#### The Brain:
Cognitive science has not been able to prove what a thought is. However, cognitive science is an interdisciplinary science that studies conscious and potentially conscious processes in which conscious and subconscious minds are explored. We have many different cognitive abilities, such as memory, language, perception, problem-solving, mental will, and many more. The only thing scientists have agreed on so far is that thought is not material (at least, it has not been proven yet).

> `A thought is an individual process (action/reaction) to one or more influences (internal/external) in which information is interpreted and linked inwardly according to our personal methodology (developed through our existing lifetimes).`

> `Consciousness describes the totality of all those mental processes by which we become aware of the external and our internal world with active observation. Therefore, when we actively observe that we are looking at a monitor full of text and can decide to change the situation if necessary and intentionally look elsewhere, we are in consciousness.`

#### The Will:
Definition of Will:
`Will is considered a descriptive construct that presupposes a conscious decision to act, and thus, it is primarily associated with rational action.`

`The mental act from which an impulse to achieve specific goals emanates and the setting of goals, and the ultimate translation of these personally or collectively made decisions into action, that is, into conscious and deliberate or even planned action.`

Definition of Fear:
`Fear is a state and the product of our imagination of the future and its consequences where the present is suppressed.`

#### The Goal:
The term goal refers to a state that lies in the future, generally changed from the present, desirable, that we aspire to fulfill. A goal is thus a defined and desired endpoint of a process. Therefore, it must be possible to determine precisely from the formulation of the goal what the desired state must be for a goal to be considered achieved. There are many different types of goals, such as:
- Quantitative goals
- Qualitative goals
- Complimentary goals
- Competing goals
- Indifferent goals
- Main goals
- Secondary goals
- and many more...

Try to make a goal and stick to it. The path to the goal is not always evident. But the path is also not static. There are multiple ways / paths to reach a goal and following just one path is tantamount to foolishnesss.

#### Decision Making:
Most often we are stuck with an either or decision scheme. Making the decision may change the path to the goal but not the goal itself. Sometimes, double tracking is better than an either or decision.

**FINALLY:**
Decide (`Decision Making`) on the goal defined in detail (`The Goal`) that you really want to achieve from your heart (`Willingness`), and that will make you happy consciously and subconsciously (`The Brain`).
