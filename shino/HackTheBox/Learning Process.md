# Learning Process
**Creation Date:** 2022-11-27    **Creation Time:** 03:24:AM

-------------------
## [[Mindset]]:
- Way of Thinking
- Think Outside the Box
- Occam's Razor
- Talent

## [[Learning Dependencies]]:
- Way of Learning
- Learning Efficiency
- Learning Types
- The Brain
- The Will
- The Goal
- Decision Making

## [[Learning Overview]]:
- Documentation
- Organization

## [[The Process]]:
- Focus
- Attention
- Comfort
- Obstacles
- Questioning
- Handling Frustration