The #Shell  prompt is of the following format:
`<username>@<hostname><current working directory>$`

The home directory for a user is marked with a tilde <`~`> and is the default folder when we log in.
`<username>@<hostname>[~]$`

The dollar sign, in this case, stands for a user. As soon as we log in as `root`, the character changes to a `hash` <`#`> and looks like this:
`root@shino:~#`

### Getting Help:
Sometimes, we might need some help when coming across a new tool. At such a time, we can make use of two ways in order to familiar ourselves with the tools:
- Help options - `<tool> --help` --> Eg., `curl --help`
- Man pages  - `man <tool>`  --> Eg., `man curl`

### System Information:

Since we will be working with many different linux systems, we need to learn the structure and the information about the system, it's processes, network configurations, users, directories, user settings, and the corresponding factors.

Here is a list of necessary tools that will help us get the above info. Most of them are installed by default.

==*whoami*== - Displays current username._
==*id*== - Returns user ID.
==*hostname*== - Sets or prints the name of the current host.
==*uname*== - Prints basic info about the OS and system hardware.
==*pwd*== - Returns working directory name.
==*ifconfig*== - Used to assign/view address to the network interface / configure network. interface parameters.
==*ip*== - Utility to show / manipulate routing, network devices, interfaces, and tunnels.
