## Top 7 Universities in Canada: (QA World University Ranking 2022)

University of Toronto - #26
McGill University - #27
University of British Columbia - #46
Universite de Montreal - #111
University of Alberta - #126
University of Waterloo - #149

## Popular Courses:
- Agricultural Sciences
- **Business Management**
- **IT and Computer Science**
- **Finance** 
- **Engineering**
- Geosciences
- Hospitality
- Human Resources
- Psychology

## Admission Requirements:
- Original copy of Previous Qualifications
- GRE or GMAT scores (For MBA and Masters courses)
- LOR and SOP
- English Proficiency: C1 Advanced, TOEFL, IELTS
- Legal Proof that you have enough funds sources to study and survive in Canada
- A well-updated CV
- As you’re planning to pursue Masters or PhD programs, you have to offer employment letters and two academic references.
- If you’re a fresher coming to India, you’ll also require Educational Credential Assessment (ECA).