# Ability to Work while Studying

### **Canada**

- As per the Canadian Student Permit, you’re **eligible to work on the campus buildings**.
- You **can also work for the employer directly associated with the university campus**. 
- To start working, you **must fulfil the eligibility criteria of your employer**. 
- As an international student, you’re **only allowed to work during your academic phase**. 
- You need to stop working after the completion of your study program.

### **UK**

- The UK student visa, also known as Tier 4 visa, **allows international students to work 20 hours a week part-time.** 
- International students residing in the UK **can work full-time during the vacation period**. 
- You **can work inside your university campus or associate with a university-affiliated employer**. 
- You **can also work outside of your campus**, but that **depends on the terms and conditions of your study program and university**.


### **Overview**

- Regarding working while studying, if you ask-” is UK better than Canada,” the answer would be ‘Yes.’ However, in Canada, Indian students get the preference from the employers, but the work opportunities are very limited.