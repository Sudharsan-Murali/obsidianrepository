## Top 7 Universities in UK: (QA World University Ranking 2022)

University of Oxford - #2
University of Cambridge - #3
Imperial College London - #7
University College London - #8
University of Edinburg - #16
University of Manchester - #27
King's College London - #35

## Popular Courses:
- Nursing
- Psychology
- Law
- **Computer Science**
- Design Studies
- **Business Administration**
- **Business Management**

## Admission Requirements:
- Students should complete 10+2 from any recognized board or institution. The minimum marks eligibility and study programs vary from desired study program and university.
-   Aspirants might be asked to submit GMAT or GRE scores for specific courses. Some universities also conduct their interview rounds and university-specific entrance examinations.
-   In the case of management courses, you should have a few years of prior work experience.
-   The English proficiency tests accepted by the UK universities are IELTS, PTE, and TOEFL. Having an IELTS score of 6.5 scores, including 6 in each band, is accepted by most UK universities.