# Cost of Living in Canada Vs. UK
**Creation Date:** 2022-11-22    **Creation Time:** 20:11:PM

---

| Major Expenses | Cost of Living Per Year (GBP) | Cost of Living Per Year (CAD) | 
|:------------|:------------:|:------------:|
| On-Campus Accomodation | 3,600 to 4,000 | 3,000 to 8,000 |
| Private Accomodation | 4,000 to 6,000 | 7,000 to 9,000 |
| Public transportation | 1,500 to 1,800 | 900 to 1,200 |
| Entertainment | 100+ (Personal Preference) | 200+ (Personal Preference) |
| Household & Other Bills | 200 to 500 | 600 to 1,000 |
| Internet Charges | 100 to 150 | 400 to 600 |
| Stationary | 500 to 1,000 | 500 to 1,500 |