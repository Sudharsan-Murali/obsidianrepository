# **Stay Back Options in UK vs Canada**

### **UK**
- The British government has recently introduced a **new graduate route**. 
- International students graduating with an **undergraduate degree or higher can stay in the UK for 24 months after completing their studies**.

### **Canada**
-   In Canada, after pursuing an undergraduate degree, the **eligible candidates can apply for the Post-graduation work permit (PGWP)**, where one can stay within Canadian country borders for work purposes.

| Duration of Course | Duration of Stay-back |
| -------------------|-------------------|
| < 8 Months | No Stay-back Allowed |
| > 8 Months to < 2 Years | As Long as Duration of Course |
| 2+ Years | 3 Years of Stay-back Allowed |