# Differences on account of various factor
##### **Creation Date:** 2022-11-20    **Creation Time:** 18:48:PM
-------------------

## **Average Tuition Fees:**
Canada: 8800 to 21800 USD
UK: 11300 to 22600 USD

1 CAD = 61.12 INR
1 GBP = 99.83 INR

---
## **Duration:**
Same for both Canada and UK: 1 to 2 Years

---
## **Eligibility Requirements:**
**Academic Creds:**
Same for both Canada and UK: Highschool, Bachelors in related fields of study
**Documents:**
Same for both Canada and UK: Passport, Visa, SOP, LOR
**Tests:**
Same for both Canada and UK: IELTS

---
## **Test Scores:**
Format: Test --> UK / Canada

GRE --> 310-315 / 300+
GMAT --> 650+ / 600+
IELTS --> 6.5 - 7.0 / 6.0+
TEOFL --> 90+ / 90+

---
## **Student Visa Fees:**
UK: 363 EUR ---> 30,614.21 INR {Rounded -> 30,700 INR}
Canada: 150 CAD (SPA)+ 85 CAD (BF) --> 9,114.85 (SPA) + 5,165.08 (BF) ---> 14,279.93 {Rounded -> 14,300 INR}

SPA - Study Permit Application
BF - Biometrics Fee

---
## **Higher Education Institutes:**
**Canada:**
As per the canadian system, 
- Colleges --> Diplomas and Certificates. 
- Universities --> Degrees.
So, need to seek admission in Universities for Post Grad.

Public and Private Unis: Have right to grant degrees like 1 yr certs and post graduate diplomas
Colleges and Institutes: 
- Offers training in  related fields of work.
- Also called as Institutes of Technology, Community Colleges, Colleges of Applied Arts

**UK:**
- Autonomous Institutions: Publicly funded institutions, run privately, or 