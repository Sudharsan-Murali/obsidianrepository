### **Student Visa for UK**

-   The full-time student visa in the UK is known as Tier 4.
-   The cost of a UK student visa is around £363. You might have to pay the same amount for the dependent visa. An international health surcharge of £705 should also be paid.
-   You may, or the visa consulate may not call you for the personal interview round.
-   In the case of a short-term study visa, you should have the intention of leaving UK borders within 30 days of completion of your course.

**Total Visa Cost = 363£ + 705£ = 1,068£ ---> 1,03,530 Rs**

### **Student Visa for Canada**

-   The interested candidates might require Canada Study Permit or SDS Visa program
-   The cost of a Canadian student visa is around 150 CAD. You also need to pay 85 CAD as biometric fees.
-   You have to submit your biometrics, but appearing for the interview round isn’t necessary. Only appeared for the interview round if asked.

**Total Visa Cost = 150CAD + 85CAD = 235 CAD ---> 14,310 Rs**

### **Documents Required for UK Student Visa**

-   A valid passport
-   Confirmation of Acceptance for Studies (CAS) reference number and other documents asked for obtaining CAS.
-   Passport-sized photographs
-   Tuberculosis screening report (if asked)
-   Assessment documentation
-   ATAS (Academic Technology Approval Scheme) clearance report
-   Proof of student loan from the bank (if you have taken a student loan)

### **Documents Required for Canada Student Visa**

-   A valid passport
-   The acceptance letter from the Designated Learning Institute (DLI)
-   You have to provide the relevant evidence that you have 10,000 CAD as financial assistance.
-   Two passport-sized photographs
-   Immigration medical examination (IME)
-   The English language proficiency exam scores
-   Proof of student loan from the bank (if you have taken a student loan)
-   Guaranteed Investment Certificate (GIC) from the Canadian financial organization (if asked)