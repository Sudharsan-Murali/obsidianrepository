## Tips:

1. Write all the time
2. Everything is knowledge
	1. This is a non-hierarchical way
	2. Nothing is unnecessary
3. Notes are irreducible. Take complete thought notes and dont reduce it to nothing
4. Knowledge is interconnected.

## Steps:
1. Take fleeting notes. Eg.,Quick handwritten notes or an audio memo, etc.,
2. Use tabs like sticky notes when reading books.
3. If something needs your thoughts to be added, add that to the book a sticky note
4. Always include page number, or the source of the info
5. Turn the fleeting notes into permanent notes
6. Notes can either be written on a paper and then transferred into the PC or can be directly input to the PC

Tags: [[KMS]], [[Learning]], [[Obsidian]]
Reference: [Youtube --> Morganeua - The fun and efficient notetaking system I use in my phd](https://www.youtube.com/watch?v=L9SLlxaEEXY)